puts "Please enter a number"
n = gets.chomp.to_i
divisor = 1
pi = 0
add = true
n.times do
  if add
    pi = pi + 4.0 / divisor
  else
    pi = pi - 4.0 / divisor
  end
  divisor = divisor + 2
  add = !add
end
puts pi
