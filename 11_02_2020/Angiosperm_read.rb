sequences = {}
name = ""
comment = ""
sequence = ""

# chomp entfernt Zeilenumbrüche
File.open("angiosperms.fasta", "r").each do |line|
	line = line.chomp
	if line.start_with?(">")
		if name != ""
			sequences[name] = sequence
			sequence = ""
		end
		name = line.delete(">")
	elsif line.start_with?(";")
		comment = line.delete(";")
	else
		sequence += line.delete("-")
	end
end

sequences[name] = sequence
puts sequence["Ginkgo"] 
puts sequences.length

sum = 0
sequences.values.each do |sequence|
	length = sequence.delete("-").size
	sum += length
end
puts (sum.to_f / sequences.length).round(3)


gc_content = 0
sequences.values.each do |sequence|
	length = sequence.delete("-").size
	gc_content += (sequence.count("G") + sequence.count("C")) / length.to_f
end
puts (gc_content.to_f / sequences.length).round(5)


File.open("Pfannkuchen.fasta", "w") do |file|
	sequences.each do |name, sequence|
		name = ">" + name
		file.puts name
		file.puts sequence.scan(/\w{0,80}/)
	end
end


