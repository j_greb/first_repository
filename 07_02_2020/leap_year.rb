puts "Please enter a year"
year = gets.chomp.to_i

if 
	(year % 4 == 0) && (year % 400 == 0)
	puts "#{year} is a leap year"
elsif 
	(year % 4 == 0) && !(year % 400 == 0)
	puts "#{year} is not a leap year"
else	
	puts "#{year} is not a leap year"
end