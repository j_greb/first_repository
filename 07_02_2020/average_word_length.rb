puts "Please enter a sentence"
sentence = gets.chomp
words = sentence.split
sum = 0

words.each do |word|
	sum += word.length
end

puts "The average word length is #{(sum.to_f / words.length).round(2)}"

