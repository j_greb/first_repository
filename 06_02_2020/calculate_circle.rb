puts "Please name the radius of the sphere you want to calculate"
radius = gets.chomp.to_f
puts "the volume of the sphere is V = #{(4 / 3) * PI * (radius ** 3)}"